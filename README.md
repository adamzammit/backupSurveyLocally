# backupSurveyLocally

Backup ALL surveys on the server as a lsa file in their own directory.

## Usage

- Create a directory outside web (otherwise everyone can get your backup file)
- Set plugin configuration
    - Set complete directory name to _Local directory_
- All surveys will be backed up by default
- Use the plugin configuration on an individual survey to exclude it from being backed up
- Save your survey locally with PHP Cli : <code>php application/commands/console.php plugin --target=backupSurveyLocally</code>
- Access your backups from the new "Download backups" menu on the main page

If you use LimeSurvey version before 3.17.8, you need to be in LimeSurvey directory for call console.php.
Then for a cron script :
````
#!/bin/bash
cd /var/www/limesurvey
php application/commands/console.php plugin --target=backupSurveyLocally
````

## Home page, copyright and support
- Copyright © 2019 Denis Chenu <https://sondages.pro> and Adam Zammit <https://acspri.org.au>(https://gitlab.com/SondagesPro/coreAndTools/backupSurveyLocally/-/graphs/master)
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
