<?php
/**
 * backupSurveyLocally : Backup your survey locally
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019 Denis Chenu <https://www.sondages.pro>
 * @license AGPL v3
 * @version 1.0.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class backupSurveyLocally extends PluginBase
{
    protected $storage = 'DbStorage';
    static protected $description = 'Backup your survey locally';
    static protected $name = 'backupSurveyLocally';

    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'localDirectory' => array(
            'type' => 'string',
            'label' => 'Local directory where backup your survey',
            'help' => 'This directory must be out of web access, but web user must be allowed to create directory and write in this directory',
        ),
    );

    /* @var string */
    private $_errorOnDir = "";

    /**
    * Add function to be used in cron event
    * @see parent::init()
    */
    public function init()
    {
        /* Action on cron */
        $this->subscribe('direct','backupSurveys');
        /* Needed config */
        $this->subscribe('beforeActivate');

        /* The survey seeting */
        $this->subscribe('beforeSurveySettings');
    $this->subscribe('newSurveySettings');

    /* Menu */
    $this->subscribe('beforeSurveyBarRender');

    /* User file download */
    $this->subscribe('newDirectRequest');
    }

    /**
     * The real action of this plugin, find all survey to be saved
     * Renaming dir one by one
     * Save surveys as LSA in the #1
     */
    public function backupSurveys()
    {
        if($this->event->get("target") != get_class()) {
            return;
        }
        if(empty($this->get('localDirectory'))) {
            throw new CException("localDirectory is not set, unable to save your survey‘s.");
        }
        /** needed for rmdirr function **/
        Yii::import('application.helpers.common_helper', true);
        /* Replace some core function */
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        Yii::import(get_class($this).'.helpers.globalsettings_helper', true);
        /** Needed function for export**/
        Yii::import('application.helpers.export_helper', true);

        $localDirectory = $this->_checkDirectory($this->get('localDirectory'));
        if(empty($localDirectory)) {
            throw new CException($this->_errorOnDir);
        }
        /* lsa is zip */
        if (!defined('PCLZIP_TEMPORARY_DIR')) {
            define('PCLZIP_TEMPORARY_DIR', $localDirectory.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR);
        }
        Yii::import('application.libraries.admin.pclzip', true);
        $oSurveysToSave = \Survey::model()->findAll();

        foreach($oSurveysToSave as $oSurvey) {
            $surveyId = $oSurvey->sid;
            $criteria = New CDbCriteria;
            $criteria->compare('plugin_id',$this->id);
            $criteria->compare(Yii::app()->getDb()->quoteColumnName('key'),'active');
            $criteria->compare(Yii::app()->getDb()->quoteColumnName('value'),'Y',true);// Partial (saved json encoded) 
            $criteria->compare(Yii::app()->getDb()->quoteColumnName('model_id'),$surveyId);
            $oSurveysToIgnore = \PluginSetting::model()->findAll($criteria);

            if (count($oSurveysToIgnore) == 0) {
                $this->_saveSurvey($surveyId,$localDirectory.DIRECTORY_SEPARATOR);
            }
        }
    }

    /** Add the settings **/
    public function beforeSurveySettings() {
        $surveyId = $this->getEvent()->get('survey');
        $this->getEvent()->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'active'=>array(
                    'type'=>'checkbox',
                    'label'=> $this->_translate('Do NOT backup this survey locally.'),
                    'htmlOptions' => array(
                        'value'=>'Y', // Use Y to use N when have inherit system
                        'uncheckValue'=>'N',// Set to N if submitted 
                    ),
                    'current' => $this->get('active', 'Survey', $surveyId,false),
                ),
            )
        ));
    }

  public function newDirectRequest()
  {
      $event = $this->event;

      //Download action initiated      
      if ($event->get('target') == get_class() && $event->get('function') == 'actionDownload') {
          $request = $event->get('request');
          $surveyId = $request->getParam('surveyId');
          $file = sanitize_filename($request->getParam('file'));

          //check for permission to download
          if(Permission::model()->hasGlobalPermission('superadmin', 'read') ||
              (Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export') && 
               Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export'))){

              $localDirectory = $this->_checkDirectory($this->get('localDirectory'));
              if(is_dir($localDirectory.DIRECTORY_SEPARATOR.$surveyId)) {
                  if (is_file($localDirectory.DIRECTORY_SEPARATOR.$surveyId.DIRECTORY_SEPARATOR.$file)) {
                      header("Content-Type: application/force-download; charset=UTF-8");
                      header("Content-Disposition: attachment; filename={$file}");
                      header("Expires: 0"); // Date in the past
                      header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
                      header("Cache-Control: must-revalidate, no-store, no-cache");
                      echo(file_get_contents($localDirectory.DIRECTORY_SEPARATOR.$surveyId.DIRECTORY_SEPARATOR.$file));
                      safedie();
                  }
              }
          }  
          throw new CHttpException(403, $this->_translate("We are sorry but you don't have permissions to do this.",'unescaped'));
     }
  }


  public function beforeSurveyBarRender()
  {
      $event = $this->getEvent();
      $surveyId = $event->get('surveyId');
      if(Permission::model()->hasGlobalPermission('superadmin', 'read') ||
        (Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export') && 
         Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export'))){

          //list all backups in directory or note that no backups created yet
          $localDirectory = $this->_checkDirectory($this->get('localDirectory'));
            
          if(is_dir($localDirectory.DIRECTORY_SEPARATOR.$surveyId)) {
          $aScanned = array_diff(scandir($localDirectory.DIRECTORY_SEPARATOR.$surveyId), array('..', '.'));
          $aMenu = array();
          if (count($aScanned) > 0) {
              foreach($aScanned as $sFile) {
              $href = Yii::app()->createUrl(
                  'plugins/direct',
                   array('plugin' => get_class($this),
                     'function' => 'actionDownload',
                     'surveyId' => $surveyId,
                     'file' => $sFile));
              $aMenuItem = array(
                  'label' => $sFile,
                  'iconClass' => 'fa fa-download',//'fa fa-edit',
                  'href' => $href);
              $aMenu[] = new \LimeSurvey\Menu\MenuItem($aMenuItem);
              }
          } else {
              $aMenu[] = new \LimeSurvey\Menu\MenuItem(
               array('label' => $this->_translate('No backups currently available'),
                 'iconClass' => 'fa fa-times'));
          }
	 $menu = new \LimeSurvey\Menu\Menu(array(
         'label' => $this->_translate('Download backups'),
         'iconClass' => 'fa fa-archive',
         'isDropDown' => true,
         'menuItems' => $aMenu,));
         $event->append('menus',array($menu));
	 }
     }
   }

    /** Save the settings **/
    public function newSurveySettings()
    {
        $oEvent = $this->event;
        foreach ($oEvent->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $oEvent->get('survey'),'');
        }
    }

    /**
     * Must set a directory before activate
     */
    public function beforeActivate()
    {
        if(empty($this->get('localDirectory'))) {
            $this->getEvent()->set('message', $this->_translate("This plugin must be configured before it can be activated."));
            $this->getEvent()->set('success', false);
            return;
        }
    }

    /**
    * @see parent::saveSettings()
    */
    public function saveSettings($settings)
    {
        if(!empty($settings['localDirectory']))
        {
            $checkLocalDirectory = $settings['localDirectory'];
            $settings['localDirectory'] = $this->_checkDirectory($settings['localDirectory']);
            if($settings['localDirectory'] && $settings['localDirectory'] != $checkLocalDirectory) {
                App()->setFlashMessage($this->_translate("The directory was updated to the real path."),'warning');
            }
            if(!$settings['localDirectory']) {
                App()->setFlashMessage($this->_errorOnDir,'danger');
            }
        }
        parent::saveSettings($settings);
    }

    /*
     * Set default when show setting
     */
    public function getPluginSettings($getValues=true)
    {
        $aPluginSettings = parent::getPluginSettings($getValues);
        $aPluginSettings['localDirectory']['help'] = CHtml::tag("p",array(),$this->_translate('This directory must be out of web access, but web user must be allowed to create directory and write in this directory.'));
        $aPluginSettings['localDirectory']['help'].= CHtml::tag("p",array(),sprintf($this->_translate('The directory of Limesurvey is : %s'),App()->getConfig('rootdir')));
        if($getValues && !empty($aPluginSettings['localDirectory']['current']) ) {
            $checkLocalDirectory = $aPluginSettings['localDirectory']['current'];
            $settings['localDirectory'] = $this->_checkDirectory($aPluginSettings['localDirectory']['current']);
            if($aPluginSettings['localDirectory']['current'] && $aPluginSettings['localDirectory']['current'] != $checkLocalDirectory) {
                App()->setFlashMessage($this->_translate("The directory was updated to the real path."),'warning');
            }
            if(!$aPluginSettings['localDirectory']['current']) {
                App()->setFlashMessage($this->_errorOnDir,'danger');
            }
        }
        return $aPluginSettings;
    }

    /**
     * Check if a directory is valid for purpose
     * @return false|string
     */
    private function _checkDirectory($localDirectory)
    {
        /* Remove the warning about open_base_dir */
        $localDirectory = @realpath($localDirectory);
        if($localDirectory === false) {
            $this->_errorOnDir = $this->_translate("The directory you set is invalid and can not be used.");
            return false;
        }
        rmdirr($localDirectory."/backupSurveyLocallyCheck");
        if(!mkdir($localDirectory."/backupSurveyLocallyCheck")) {
            $this->_errorOnDir = $this->_translate("Unable to create directory in directory set.");
            return false;
        }
        //~ if(!mkdir($localDirectory."/backupSurveyLocallyCheck/dir")) {
            //~ $this->_errorOnDir = $this->_translate("Able to create directory in directory set, but unable to create a directory in this new directory.");
            //~ return false;
        //~ }
        if(!touch($localDirectory."/backupSurveyLocallyCheck/file")) {
            $this->_errorOnDir = $this->_translate("Able to create directory in directory set, but unable to create a file in the directory created.");
            return false;
        }
        if(!rmdirr($localDirectory."/backupSurveyLocallyCheck")) {
            $this->_errorOnDir = $this->_translate("Able to create directory in directory set, but unable to delete.");
            return false;
        }
        if(!is_dir($localDirectory.DIRECTORY_SEPARATOR."tmp")) {
            mkdir($localDirectory.DIRECTORY_SEPARATOR."tmp");
        }
        return $localDirectory;
    }

    /**
     * @see parent::gT for LimeSurvey 3.0
     * @param string $sToTranslate The message that are being translated
     * @param string $sEscapeMode unescaped by default
     * @param string $sLanguage use current language if is null
     * @return string
     */
    private function _translate($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if(is_callable($this, 'gT')) {
            return $this->gT($sToTranslate,$sEscapeMode,$sLanguage);
        }
        return $sToTranslate;
    }

    /**
     * Fix LimeSurvey command function and add own needed function 
     * @todo : find way to control API
     * OR if another plugin already fix it
     */
    private function _LsCommandFix()
    {
        /* Bad autoloading in command */
        include_once(dirname(__FILE__)."/DbStorage.php");

    }

    /**
     * Save a survey as lsa in directory
     * @param integer $surveyId survey id
     * @param string $dirName directory name (must exist)
     * @return boolean true is survey was saved.
     */
    private function _saveSurvey($surveyId,$dirName)
    {
        $survey = Survey::model()->findByPk($surveyId);
        if(empty($survey)) {
            /* @todo : delete PluginSettings ? */
            return;
        }

        /* This part is copy /paste from export->_exportarchive core function */
        $aSurveyInfo = getSurveyInfo($surveyId);

        if(!is_dir($dirName.DIRECTORY_SEPARATOR.$surveyId)) {
               mkdir($dirName.DIRECTORY_SEPARATOR.$surveyId);
        }

        $aZIPFileName = $dirName.DIRECTORY_SEPARATOR.$surveyId.DIRECTORY_SEPARATOR.'survey-archive-'.$surveyId.'-'.date("Y-m-d-His").'.lsa';
        $sLSSFileName = $dirName.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR."lss".randomChars(30);
        $sLSRFileName = $dirName.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR."lsr".randomChars(30);
        $sLSTFileName = $dirName.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR."lst".randomChars(30);
        $sLSIFileName = $dirName.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR."lsi".randomChars(30);

        Yii::import('application.libraries.admin.pclzip', true);
        $zip = new PclZip($aZIPFileName);

        file_put_contents($sLSSFileName, surveyGetXMLData($surveyId));
        $this->_addToZip($zip, $sLSSFileName, 'survey_'.$surveyId.'.lss');
        unlink($sLSSFileName);

        if ($survey->active == "Y") {
            getXMLDataSingleTable($surveyId, 'survey_'.$surveyId, 'Responses', 'responses', $sLSRFileName, false);
            $this->_addToZip($zip, $sLSRFileName, 'survey_'.$surveyId.'_responses.lsr');
            unlink($sLSRFileName);
        }

        if (tableExists("{{tokens_".$surveyId."}}")) {
            getXMLDataSingleTable($surveyId, 'tokens_'.$surveyId, 'Tokens', 'tokens', $sLSTFileName);
            $this->_addToZip($zip, $sLSTFileName, 'survey_'.$surveyId.'_tokens.lst');
            unlink($sLSTFileName);
        }

        if (tableExists("{{survey_".$surveyId."_timings}}")) {
            getXMLDataSingleTable($surveyId, 'survey_'.$surveyId.'_timings', 'Timings', 'timings', $sLSIFileName);
            $this->_addToZip($zip, $sLSIFileName, 'survey_'.$surveyId.'_timings.lsi');
            unlink($sLSIFileName);
        }
        return true;
    }
    /**
     * Function copy from export
     * @see \export->_addToZip
     * @param PclZip $zip
     * @param string $name
     * @param string $full_name
     */
    private function _addToZip($zip, $name, $full_name)
    {
        $check = $zip->add(
            array(
                array(
                    PCLZIP_ATT_FILE_NAME => $name,
                    PCLZIP_ATT_FILE_NEW_FULL_NAME => $full_name
                )
            )
        );
        if ($check == 0) {
            throw new CException("PCLZip error : ".$zip->errorInfo(true));
        }
    }

}
